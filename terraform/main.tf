terraform {
  backend "s3" {
    bucket = "terraform-state-ghost-blog"
    key    = "terraform.tf-state"
    region = "us-east-1"
  }
}

data "aws_ami" "centos" {
  most_recent = true

  filter {
    name   = "name"
    values = ["CentOS Linux 7 x86_64 HVM EBS *"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["679593333241"] # Canonical
}

resource "tls_private_key" "dev" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "${tls_private_key.dev.public_key_openssh}"
}

resource "aws_security_group" "allow_ssh" {
  name        = "allow_all"
  description = "Allow inbound SSH traffic from my IP"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Allow SSH"
  }
}

resource "aws_instance" "ghost_blog" {
  ami             = "${data.aws_ami.centos.id}"
  instance_type   = "t2.micro"
  key_name        = "${aws_key_pair.deployer.key_name}"
  security_groups = ["${aws_security_group.allow_ssh.name}"]

  tags = {
    Name = "ghost-blog"
    Type = "webserver"
  }
}

resource "local_file" "dev_private_key" {
  content  = "${tls_private_key.dev.private_key_pem}"
  filename = "${path.module}/../generated_keys/id_rsa"
  
  provisioner "local-exec" {
    command = "chmod 600 ${path.module}/../generated_keys/id_rsa"
  }
}
