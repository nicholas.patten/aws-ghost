variable "aws_ec2_access_key" {
  type = "string"
}

variable "aws_ec2_secret_key" {
  type = "string"
}